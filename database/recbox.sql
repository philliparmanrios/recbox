/*
SQLyog Ultimate v11.33 (64 bit)
MySQL - 5.7.14 : Database - recbox
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`recbox` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `recbox`;

/*Table structure for table `administrators` */

DROP TABLE IF EXISTS `administrators`;

CREATE TABLE `administrators` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `middle_name` varchar(50) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(99) NOT NULL,
  `phone_number` varchar(16) NOT NULL,
  `position` varchar(50) NOT NULL,
  `schedule` date DEFAULT NULL,
  `remarks` varchar(16) DEFAULT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL,
  `admin` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;

/*Data for the table `administrators` */

insert  into `administrators`(`id`,`first_name`,`last_name`,`middle_name`,`username`,`password`,`phone_number`,`position`,`schedule`,`remarks`,`created_at`,`updated_at`,`admin`) values (1,'Phillip Arman','Rios','Tahanlangit','armanriri','$2y$10$wJmLoxdbPvOX2BhvKGadM.2sEsILZYTgnIPD/eIa8WvL1i8pcCBbS','639236386510','CEO','2019-07-18','Passed','2019-07-09 01:16:05','2019-07-09 01:16:05',1),(42,'Judelyn','Rubia','','639060175467','$2y$10$ayMhYAzJItHA1CwZhKZmFeN778EmmLNZb0Jc5W2IBbz0uPLz.EeoG','639060175467','CEO','2019-08-27','Hired','2019-08-08 13:25:47','2019-08-08 18:38:33',0);

/*Table structure for table `applicants` */

DROP TABLE IF EXISTS `applicants`;

CREATE TABLE `applicants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `middle_name` varchar(50) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(99) NOT NULL,
  `phone_number` varchar(16) NOT NULL,
  `desired_position` varchar(50) NOT NULL,
  `schedule` date DEFAULT NULL,
  `remarks` varchar(16) DEFAULT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

/*Data for the table `applicants` */

insert  into `applicants`(`id`,`first_name`,`last_name`,`middle_name`,`username`,`password`,`phone_number`,`desired_position`,`schedule`,`remarks`,`created_at`,`updated_at`) values (20,'Phillip Arman','Rios','','639236386510','$2y$10$bu7ceJpFuPreUhHih5wX3Os5P8CYPPkCsd3DcW00sD9VJY3iaQb5G','639236386510','CEO','2019-08-12','Hired','2019-08-08 18:00:23','2019-08-08 18:27:38'),(19,'Phillip Arman','Rios','','639060175469','$2y$10$z2p4OkCE6ndb/oKKz64E/uG1aH4lH4zEJU5amKXmKWaSjUYG4plX6','639060175469','CEO','2019-08-21','Hired','2019-08-08 17:58:50','2019-08-08 18:31:51');

/*Table structure for table `messages` */

DROP TABLE IF EXISTS `messages`;

CREATE TABLE `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `to` varchar(20) NOT NULL,
  `from` varchar(99) NOT NULL,
  `message` varchar(500) NOT NULL,
  `sent_at` datetime NOT NULL,
  `date_sent` date NOT NULL,
  `sender` varchar(20) NOT NULL,
  `sender_name` varchar(99) NOT NULL,
  `read` tinyint(1) NOT NULL,
  `seen` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

/*Data for the table `messages` */

insert  into `messages`(`id`,`to`,`from`,`message`,`sent_at`,`date_sent`,`sender`,`sender_name`,`read`,`seen`) values (14,'639236386510','Arman Corp.','Bai','2019-08-08 15:46:47','2019-08-08','639060175467','Judelyn Rubia',1,'2019-08-08 15:53:31'),(13,'639060175467','Arman Corp.','Hello!','2019-08-08 13:41:50','2019-08-08','639236386510','Phillip Arman Rios',1,'2019-08-08 16:53:21'),(15,'639060175467','Arman Corp.','Unsa mn bai?','2019-08-08 15:51:55','2019-08-08','639236386510','Phillip Arman Rios',1,'2019-08-08 16:51:54'),(16,'639060175467','Arman Corp.','Unsa mn bai?','2019-08-08 15:52:28','2019-08-08','639236386510','Phillip Arman Rios',0,NULL),(17,'639060175467','Arman Corp.','Yo!','2019-08-08 15:53:36','2019-08-08','639236386510','Phillip Arman Rios',1,'2019-08-08 16:51:31'),(18,'639236386510','Arman Corp.','Musta nmn?','2019-08-08 15:54:37','2019-08-08','639060175467','Judelyn Rubia',0,NULL);

/*Table structure for table `recruiters` */

DROP TABLE IF EXISTS `recruiters`;

CREATE TABLE `recruiters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `middle_name` varchar(50) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `phone_number` varchar(16) NOT NULL,
  `schedule` date DEFAULT NULL,
  `remarks` varchar(16) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `recruiters` */

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(250) CHARACTER SET latin1 NOT NULL,
  `email` varchar(250) CHARACTER SET latin1 NOT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`username`,`email`,`created_at`,`updated_at`) values (1,'arman','arman@test.com','2019-06-29 16:54:59','2019-06-29 16:54:59'),(2,'rios','rios@test.com','2019-06-29 16:55:23','2019-06-29 16:55:23');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
