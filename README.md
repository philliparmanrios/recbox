1. How to access?
- Go to 'https://localhost/recbox/public' it will direct you to the home or starting page of the site.
2. Steps on adding a view and controller.
    1. go to the views directory.
    2. create new folder and create a new php file for the view of the site. PS: it only consist of HTML codes and less php codes.
    3. then go to the controllers directory.
    4. add new php file. Better if you named it after based on the folder that you created in the views directory.
    5. copy past this code.
    
    ```
    <?php

    class nameoftheclass extends Controller {
    
      public function nameofthefunction() {
      
        
      }
    }
    ```
    6. then add this code inside of the nameofthefunction function.
    
    1 - name of the folder that you created.
    2 - name of the file that you created. so that it renders the view page of your site.
    ```
        $this->view(1/2, []);
    ```
    7. then go to the core directory.
    8. click the Router.php and you see there is a code that looks like this.
    ```
    protected $_uri = array(
        '/' => 'home',
        'admin/' => 'admin',
        'admin/profile' => 'admin-profile',
        'admin/add' => 'admin-add',
        'admin/listAdmin' => 'list-admin'
    );
    ```
    9. you must add an route here in order to easy access the routes within the page.
    10. 'nameoftheclass/nameofthefunction' => 'nameoftheurl' PS: any name will do however the url must be the 'class/function'.
    11. and Success! you just add a view and controller with adding router in the site.
    12. You can access through this 'https://localhost/recbox/public/nameoftheclass/nameofthefunction'.

    NOTE!
    number 8 - 11 is no longer in use. so never mind that

    ANOTHER NOTE!
    if you encounter error in sending a Text Message. Either you contact me or 
    you put this in your system.
    1. go to the files directory (same place database directory was placed).
    2. as you can see there is a file named 'cacert.pem'.
    3. put that file in the bin/php/php7.0.10/extras/ssl
    4. as for me because I was using php7.0.10 version but if you only have version lower than than, you still can able to put that file on.
    5. then go to the php.ini. you can find that in the icon of wampserver lower right of the navbar
    6. left click it and go to PHP and you can find the php.ini
    7. search "curl.cainfo"
    8. at first you see it has a semicolomn ";curl.cainfo" like this. it means that line is commented
    9. remove the semicolomn and paste this line. ' curl.cainfo = "C:\wamp64\bin\php\php7.0.10\extras\ssl\cacert.pem" ' like this.
    10. then after this, try send a text message. if there still a problem contact me.