<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <link href="../css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
  <a href="<?= $this->route('admin/profile') ?>">Back</a>
  <br><br>
  <table style="width:50%">
    <tr>
      <th>Admin name</th>
      <th>Desired Position</th> 
      <th>Phone #</th>
    </tr>
    <?php foreach ($data['applicants'] as $applicant) : ?>
      <tr>
        <td><?= $applicant->first_name . ' ' . $applicant->last_name ?></td>
        <td><?= $applicant->desired_position ?></td>
        <td><?= $applicant->phone_number ?></td>
      </tr>
    <?php endforeach; ?>
  </table>
</body>
</html>