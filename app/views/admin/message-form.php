<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <link href="../css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
  <a href="<?= $this->route('admin/profile') ?>">Back</a>
  <br><br>
  <form action="<?= $this->route('admin/send') ?>" method="POST">
    <label for="to">To: </label>
    <select name="code">
      <option value="63">Phil +63</option>
    </select>
    <input type="text" name="to" placeholder="Example: 9060175467">
    <br><br>
    <label for="from">From: </label>
    <input type="text" name="from">
    <br><br>
    <label for="message">Message: </label><br>
    <textarea name="message" id="" cols="100" rows="10"></textarea>
    <br><br>
    <button name="send">Send</button>
  </form>
</body>
</html>