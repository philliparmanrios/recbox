<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <link href="../css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
  <a href="<?= $this->route('admin/inbox') ?>">Back</a>
  <br><br>
  <label for="From">From: </label>
  <br>
  <?= $data['message']->from ?>
  <br><br>
  <label for="number">To: </label>
  <br>
  <?= $data['message']->to ?>
  <br><br>
  <label for="Message">Message: </label>
  <br>
  <?= $data['message']->message ?>
  <br><br>
  <a href="<?= $this->route('admin/reply', '?id=' . $data['message']->id . '&sender=' . $data['message']->sender . '&from=' . $data['message']->from) ?>">Reply</a>
</body>
</html>