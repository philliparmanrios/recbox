<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <link href="../css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>

  <div class="web-sandbox">
    <div class="sandbox_container header-contain">
      <p><span class="span-yellow">REC</span>BOX</p>
    </div>
    <div class="sandbox_container form-contain">
      <div class="body_form">
        <div class="info-text_contain login-text">
          <div class="acc-icon admin-acc_icon">
          </div>
          <h3>Administrator login</h3>
        </div>
        <?php if (isset($_GET['m'])) : ?>
          Error
        <?php endif;?>
        <form action="<?= $this->route('admin/profile') ?>" method="POST">
          <!-- <label for="username">Username: </label> -->
          <input type="text" name="username" placeholder="username" required>
          
          <!-- <label for="password">Password: </label> -->
          <input type="password" name="password" placeholder="password" required>
          <br>
          <button name="login">Login</button>
        </form>
      </div>
      <a href="<?= $this->route('/') ?>">
        <p>← go back</p>
      </a>
    </div>
    <div class="sandbox_container footer">
      <!-- FOOTER HERE -->
      <span>Designed by a designer wannabe.</span>
    </div>
  </div>

</body>
</html>