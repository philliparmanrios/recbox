<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <link href="../css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
  <a href="<?= $this->route('applicant/profile') ?>">Back</a>
  <br><br>
  <form action="<?= $this->route('applicant/schedule') ?>" method="POST">
    <label for="id">ID: </label>
    <input type="text" name="id" value="<?= $data['id'] ?>">
    <br><br>
    <label for="from">Set Schedule: </label>
    <input type="date" name="schedule">
    <br><br>
    <button name="set">Set</button>
  </form>
</body>
</html>