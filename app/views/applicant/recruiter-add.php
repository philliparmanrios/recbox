<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <link href="../css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
  <?php if ($data['username_exist']) : ?>
    <span style="color:red">Phone number already exist</span><br>
  <?php endif;?>
  <a href="<?= $this->route('applicant/profile') ?>">Back</a>
  <br><br>
  <form action="<?= $this->route('applicant/addRecruiter') ?>" method="POST">  
    <label for="first_name">First name: <span style="color:red"><?= $data['first_nameErr']?></span> </label>
    <input type="text" name="first_name">
    <br><br>
    <label for="last_name">Last name: <span style="color:red"><?= $data['last_nameErr']?></span> </label>
    <input type="text" name="last_name">
    <br><br>
    <label for="middle_name">Middle name: </label>
    <input type="text" name="middle_name">
    <br><br>
    <label for="phone_number">Phone Number: <span style="color:red"><?= $data['phone_numberErr']?></span> </label>
    <input type="text" name="phone_number" placeholder="Example: 639060175467">
    <br><br>
    <label for="first_name">Password: </label>
    <input type="password" name="password">
    <br><br>
    <button name="save">Save</button>
  </form>
</body>
</html>