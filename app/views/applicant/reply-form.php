<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <link href="../css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
  <a href="<?= $this->route('applicant/inboxMessage', $data['id']) ?>">Back</a>
  <br><br>
  <form action="<?= $this->route('applicant/reply') ?>" method="POST">
    <label for="to">To: </label>
    <input type="text" name="to" value="<?= $data['number'] ?>">
    <br><br>
    <label for="from">from: </label>
    <input type="text" name="from" value="<?= $data['from'] ?>">
    <br><br>
    <label for="message">Message: </label><br>
    <textarea name="message" id="" cols="100" rows="10"></textarea>
    <br><br>
    <button name="send">Send</button>
  </form>
</body>
</html>