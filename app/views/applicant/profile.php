<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <link href="../css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>

  <div class="web-sandbox superadmin-page">
    <!-- <div class="sandbox_container header-ribbon">
      <div class="sandbox_container-box header_top-ribbon">
        <div class="ribbon_option-box">
          <div class="option_item inbox">
            <span>Inbox</span>
          </div>
          <div class="option_item logout">
            <span>Logout</span>
          </div>
        </div>
        USERNAME OF SUPERADMIN
        <span class="header_page-info">(Super) admin dashboard</span>
      </div>
    </div> -->
    <div class="sandbox_container container_info-content">
      <div class="sandbox_container-box user-info_contain">
        <!-- <div class="superadmin-options_box">
          <a href="">
            <div class="superadmin-options_item">
              <span>Manage administrators</span>
            </div>
          </a>
        </div> -->

        <div class="profile-info_contain">
          <div class="info-photo">
          </div>
          <div>
            <label for="name" id="admin-username">Name: </label>
              <?= $data['user']->first_name . ' ' . $data['user']->last_name ?>
              <br>
              <label for="position" class="admin-info">Position: </label>
              <?= $data['user']->desired_position ?>
              <br>
              <label for="phone" class="admin-info">Mobile: </label>
              <?= $data['user']->phone_number ?>
            <br>
          </div>
        </div>

        <div class="user-account-navigation">
          <div class="navi-item">
            
          </div>
          <div class="navi-item">
            <form action="<?= $this->route('applicant/profile') ?>" method="POST">
              <button name="logout">Logout</button>
            </form>
          </div>
        </div>
      </div>
    </div>

    <div class="sandbox_container container_table-contain">
      <div class="sandbox_container-box table-contain">
        <div class="applicant-view-header">
          <h2>Administrators</h2>
          <a href="<?= $this->route('applicant/listAdmins') ?>">View All Admins</a>
          <!-- <a href="<?= $this->route('applicant/addRecruiter') ?>">Add Recruiter</a> -->
          <br>
          <a href="<?= $this->route('applicant/send') ?>">Send SMS</a>
          <br>
          <a href="<?= $this->route('applicant/inbox') ?>">Inbox <?= $data['message-count'] ?> </a>
          <br>
          <a href="<?= $this->route('applicant/sent') ?>">Sent </a>
        </div>
        <br>
        <table>
          <tr>
            <th>Name</th>
            <th>Mobile</th>
            <th>Scheduling</th>
            <th>Remarks</th>
          </tr>
          <?php foreach ($data['administrator'] as $administrator) : ?>
            <tr>
              <td><?= $administrator->first_name . ' ' . $administrator->last_name ?></td>
              <td><?= $administrator->phone_number ?></td>
              <td>
                <?= ($administrator->schedule != null) ? $administrator->schedule : '(Not yet schedule)'; ?>
                <a href="<?= $this->route('applicant/schedule', $administrator->id) ?>" style="color: blue">Set</a>
              </td>
              <td><?= $administrator->remarks ?></td>
            </tr>
          <?php endforeach; ?>
        </table>
      </div>
    </div>
  </div>

</body>
</html>