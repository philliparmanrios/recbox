<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <link href="../css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
  <a href="<?= $this->route('applicant/profile') ?>">Back</a>
  <br><br>
  <table style="width:50%">
    <tr>
      <th>From</th> 
      <th>Sent at</th>
      <th>View message</th>
    </tr>
    <?php foreach ($data['messages'] as $message) : ?>
      <tr>
        <td><?= $message->from ?></td>
        <td><?= $message->sent_at ?></td>
        <td><a href="<?= $this->route('applicant/sentMessage', $message->id) ?>">View</a></td>
      </tr>
    <?php endforeach; ?>
  </table>
</body>
</html>