<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <link href="css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>

  <div class="web-sandbox">
    <div class="sandbox_container header-contain">
      <p><span class="span-yellow">REC</span>BOX</p>
    </div>
    <div class="sandbox_container button-contain">
      <div class="info-text_contain">
        <h2>Sign in as an...</h2>
      </div>

      <a href="<?= $this->route('admin/') ?>">
        <div class="button_select admin">
          <div class="button-icon admin-icon">
          </div>
          <span>Administrator</span>
        </div>
      </a>
      <a href="<?= $this->route('applicant/') ?>">
        <div class="button_select applicant">
          <div class="button-icon applicant-icon">
          </div>
          <span>Applicant</span>
        </div>
      </a>
    </div>
    <div class="sandbox_container footer">
      <!-- FOOTER HERE -->
      <span>CALLBOX 2019.</span>
    </div>
  </div>

</body>
</html>