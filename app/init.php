<?php

// Composer autoloader
require_once '../vendor/autoload.php';

// Load database
require_once 'database.php';

// Load core application
require_once 'core/App.php';
require_once 'core/Router.php';
require_once 'core/Controller.php';