<?php

class App {

  protected $controller = 'homeController';

  protected $method = 'index';

  protected $params = [];

  public function __construct() {
    
    // Parse URL.
    $url = $this->parseUrl();

    if (file_exists('../app/controllers/' . $url[0] . 'Controller.php')) {

      // Change the default controller.
      $this->controller = $url[0] . 'Controller';

      // remove the $url[0] from the array.
      unset($url[0]);
    }

    require_once '../app/controllers/' . $this->controller . '.php';

    $this->controller = new $this->controller;

    // checks if the url has a method and access it if exist.
    if (isset($url[1])) {

      if (method_exists($this->controller, $url[1])) {
        
        // Change the default method
        $this->method =  $url[1];

        // remove the method or url[1] from the array.
        unset($url[1]);
      }
    }

    $this->params = $url ? array_values($url) : [];

    call_user_func_array([$this->controller, $this->method], $this->params);
  }

  public function parseUrl() {
    
    if (isset($_GET['url'])) {

      // Filter URL
      return $url = explode('/', filter_var(rtrim($_GET['url'], '/'), FILTER_SANITIZE_URL));
    }
  }
}