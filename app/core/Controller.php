<?php

class Controller extends Router {
  
  public function model($model) {

    // access model from the models directory.
    require_once '../app/models/' . $model . '.php';
    return new $model();
  }

  public function view($view, $data) {
  
    // access view from the views directory.
    require_once '../app/views/' . $view . '.php';
  }
}