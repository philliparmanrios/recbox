<?php

class Router {

  // domain for the site. example: www.test.com
  protected $domain = '/recbox/public';
 
  public function route($uri, $param='') {

    if (!empty($param)) {

      echo $this->domain . "/" .  $uri . '/' . $param;
    } else {

      if ($uri == '/') {

        echo $this->domain;
      }else {

        echo $this->domain . "/" . $uri; 
      }
    }
  }

  public function redirect($uri, $m='') {

    if ($uri == '/') {

      if ($m != '') {

        header('Location: ' . $this->domain . '?m=' . $m);
      }else {

        header('Location: ' . $this->domain);
      }
    }else {

      if ($m != '') {

        header('Location: ' . $this->domain . '/' . $uri . '?m=' . $m);
      }else {

        header('Location: ' . $this->domain . '/' . $uri);
      }
    }
  }
  
}