<?php

// start session
session_start();

class AdminController extends Controller {

  public function index() {
    
    $this->view('admin/login', []);
    
  }

  public function profile() {
    
    // If the user is signing out.
    if (isset($_POST['logout'])) {

      session_destroy();

      $this->redirect('/', 'logout');
    }

    /** Access tables **/

    // Access table for Administrators.
    $this->model('Administrator');

    // Access table for Applicants
    $this->model('Applicant');

    // Access table for Messages
    $this->model('Message');

    /** Queries **/

    // Get list of applicants
    $applicants = Applicant::orderBy('created_at', 'desc')->get();
    
    // If the user is signing in.
    if ( isset($_POST['login']) && $_SERVER["REQUEST_METHOD"] == "POST" ) {

      $username = $_POST['username'];
      $password = $_POST['password'];

      $user = Administrator::where([
        'username' => $username
      ])->first();

      // Get inbox count
      $message_count = Message::where('read', 0)->where('to', $user->phone_number)->count();

      // Checks if the username is in the system and verify password.
      if ( !password_verify($password, $user->password) && empty($user) ) {
        
        // Redirects to the login page of the Admin.
        $this->redirect('admin/', 'login-error');
      } else {

        // check if the user is admin.
        // if confirm admin, redirects to the admin profile.
        // if not, redirects to the another admin profile.
        if($user->admin == true) {

          // user id of the super admin
          $_SESSION["user"] = $user->id;

          // user's name
          $_SESSION["name"] = $user->first_name . ' ' . $user->last_name ;

          // user's number
          $_SESSION["phone_number"] = $user->phone_number;

          // admin = 1
          $_SESSION["admin"] = $user->admin;

          $this->view('admin/profile-admin', [
            'user' => $user,
            'applicants' => $applicants,
            'message-count' => $message_count
          ]);
        }else {

          // user id of the admin or sub admin
          $_SESSION["user"] = $user->id;

          // user's name
          $_SESSION["name"] = $user->first_name . ' ' . $user->last_name ;

          // user's number
          $_SESSION["phone_number"] = $user->phone_number;

          // admin = 0
          $_SESSION["admin"] = $user->admin;

          $this->view('admin/profile', [
            'user' => $user,
            'applicants' => $applicants,
            'message-count' => $message_count
          ]);
        }
      }
    }

    // Checks authentication
    if ( !isset($_SESSION['user']) ) {
      $this->redirect('/');
    }
    
    // For redirecting to the profile page or user's main page.
    if (!empty($_SESSION['user'])) {

      // Find user using the session set by the user when logged in.
      $user = Administrator::find($_SESSION['user']);

      // Get inbox count
      $message_count = Message::where('read', 0)->where('to', $_SESSION['phone_number'])->count();

      // if admin, shows the sub admins
      if ($_SESSION['admin'] == true) {

        $this->view('admin/profile-admin', [
          'user' => $user,
          'applicants' => $applicants,
          'message-count' => $message_count
        ]);
      } 
      // if not, shows table of the recruiters or applicants
      else {

        $this->view('admin/profile', [
          'user' => $user,
          'applicants' => $applicants,
          'message-count' => $message_count
        ]);
      }
    }

  }

  public function addAdmin() {

    // Checks authentication
    if ( !isset($_SESSION['user']) ) {
      $this->redirect('/');
    }

    // define variables and set to empty values
    // For errors
    $first_nameErr = $last_nameErr = $middle_nameErr = "";
    $positionErr = $phone_numberErr = $passwordErr = "";

    // empty values
    $first_name = $last_name = $middle_name = "";
    $position = $phone_number = $password = "";

    // for checking of number of existing usernames
    $username_exist = false;

    if ( isset($_POST['save']) && $_SERVER["REQUEST_METHOD"] == "POST" ) {

      require_once '../app/services/filters.php';

      if (empty($_POST["first_name"])) {
        
        $first_nameErr = "First name is required";
      } else {
        
        $first_name = test_input($_POST["first_name"]);
        
        // check if name only contains letters and whitespace
        if (!preg_match("/^[a-zA-Z ]*$/",$first_name)) {
        
          $first_nameErr = "Only letters and white space allowed"; 
        }
      }

      if (empty($_POST["last_name"])) {
        
        $last_nameErr = "Last name is required";
      } else {
        
        $last_name = test_input($_POST["last_name"]);
        
        // check if name only contains letters and whitespace
        if (!preg_match("/^[a-zA-Z ]*$/",$last_name)) {
        
          $last_nameErr = "Only letters and white space allowed"; 
        }
      }

      if (empty($_POST["position"])) {
        
        $positionErr = "Position is required";
      } else {
        
        $position = test_input($_POST["position"]);
        
        // check if name only contains letters and whitespace
        if (!preg_match("/^[a-zA-Z ]*$/",$position)) {
        
          $positionErr = "Only letters and white space allowed"; 
        }
      }

      $username = test_input($_POST['phone_number']);
      $phone_number = test_input($_POST['phone_number']);
      $password = password_hash($_POST['password'], PASSWORD_DEFAULT);
      
      $this->model('Administrator');
      $this->model('Applicant');

      $verify_admin = Administrator::where('username', $username)->first();
      $verify_applicant = Applicant::where('username', $username)->first();

      if ((empty($verify_admin)) &&
          (empty($verify_applicant)) &&
          !empty($_POST['first_name']) && 
          !empty($_POST['last_name']) && 
          !empty($_POST['position']) && 
          !empty($_POST['phone_number'])) {

        // create new admin
        Administrator::create([
          'username' => $username,
          'password' => $password,
          'first_name' => $first_name,
          'last_name' => $last_name,
          'middle_name' => $middle_name,
          'position' => $position,
          'phone_number' => $phone_number,
          'admin' => 0
        ]);

        $this->redirect('admin/profile');
      }else {

        if (!empty($verify_admin) || !empty($verify_applicant)) {
          $username_exist = true;
        }

        $this->view('admin/admin-add', [
          'first_nameErr' => $first_nameErr,
          'last_nameErr' => $last_nameErr,
          'positionErr' => $positionErr,
          'phone_numberErr' => $phone_numberErr,
          'username_exist' => $username_exist
        ]); 
      }

    }else {

      $this->view('admin/admin-add', [
        'first_nameErr' => $first_nameErr,
        'last_nameErr' => $last_nameErr,
        'positionErr' => $positionErr,
        'phone_numberErr' => $phone_numberErr,
        'username_exist' => $username_exist
      ]); 
    }

  }

  public function listAdmins() {

    // Checks authentication
    if ( !isset($_SESSION['user']) ) {
      $this->redirect('/');
    }

    $this->model('Administrator');

    $administrators = Administrator::orderBy('created_at', 'desc')->get();

    $this->view('admin/list-admins', [
      'administrators' => $administrators
    ]);

  }

  public function addApplicant() {

    // Checks authentication
    if ( !isset($_SESSION['user']) ) {
      $this->redirect('/');
    }

    // define variables and set to empty values
    // For errors
    $first_nameErr = $last_nameErr = $middle_nameErr = "";
    $desired_positionErr = $phone_numberErr = $passwordErr = "";

    // empty values
    $first_name = $last_name = $middle_name = "";
    $desired_position = $phone_number = $password = "";

    // for checking of number of existing usernames
    $username_exist = false;

    if ( isset($_POST['save']) && $_SERVER["REQUEST_METHOD"] == "POST" ) {

      require_once '../app/services/filters.php';

      if (empty($_POST["first_name"])) {

        $first_nameErr = "First name is required";
      } else {

        $first_name = test_input($_POST["first_name"]);
        
        // check if name only contains letters and whitespace
        if (!preg_match("/^[a-zA-Z ]*$/",$first_name)) {
        
          $first_nameErr = "Only letters and white space allowed"; 
        }
      }

      if (empty($_POST["last_name"])) {
        
        $last_nameErr = "Last name is required";
      } else {
        
        $last_name = test_input($_POST["last_name"]);
        
        // check if name only contains letters and whitespace
        if (!preg_match("/^[a-zA-Z ]*$/",$last_name)) {
        
          $last_nameErr = "Only letters and white space allowed"; 
        }
      }

      if (empty($_POST["desired_position"])) {
        
        $desired_positionErr = "Desired position is required";
      } else {
        
        $desired_position = test_input($_POST["desired_position"]);
        
        // check if name only contains letters and whitespace
        if (!preg_match("/^[a-zA-Z ]*$/",$desired_position)) {
        
          $desired_positionErr = "Only letters and white space allowed"; 
        }
      }

      $username = $_POST['phone_number'];
      $phone_number = $_POST['phone_number'];
      $password = password_hash($_POST['password'], PASSWORD_DEFAULT);
      
      $this->model('Administrator');
      $this->model('Applicant');

      $verify_admin = Administrator::where('username', $username)->first();
      $verify_applicant = Applicant::where('username', $username)->first();
      
      if ((empty($verify_admin)) &&
          (empty($verify_applicant)) &&
          !empty($_POST['first_name']) && 
          !empty($_POST['last_name']) && 
          !empty($_POST['desired_position']) && 
          !empty($_POST['phone_number'])) {
            
        // create new admin
        Applicant::create([
          'username' => $username,
          'password' => $password,
          'first_name' => $first_name,
          'last_name' => $last_name,
          'middle_name' => $middle_name,
          'desired_position' => $desired_position,
          'phone_number' => $phone_number
        ]);

        $this->redirect('admin/profile');
      }else {

        if (!empty($verify_admin) || !empty($verify_applicant)) {
          $username_exist = true;
        }

        $this->view('admin/applicant-add', [
          'first_nameErr' => $first_nameErr,
          'last_nameErr' => $last_nameErr,
          'desired_positionErr' => $desired_positionErr,
          'phone_numberErr' => $phone_numberErr,
          'username_exist' => $username_exist
        ]); 
      }

    }else {

      $this->view('admin/applicant-add', [
        'first_nameErr' => $first_nameErr,
        'last_nameErr' => $last_nameErr,
        'desired_positionErr' => $desired_positionErr,
        'phone_numberErr' => $phone_numberErr,
        'username_exist' => $username_exist
      ]); 
    }

  }

  public function listApplicants() {

    // Checks authentication
    if ( !isset($_SESSION['user']) ) {
      $this->redirect('/');
    }

    $this->model('Applicant');

    $applicants = Applicant::orderBy('created_at', 'desc')->get();

    $this->view('admin/list-applicants', [
      'applicants' => $applicants
    ]);

  }

  public function send() {
    require_once "../vendor/autoload.php";

    try {

      if ( isset($_POST['send']) && $_SERVER["REQUEST_METHOD"] == "POST" ) {
      
        if (!empty($_POST['to'])) {
          
          $basic  = new \Nexmo\Client\Credentials\Basic('af4a0027', 'YJFj84sg3Ur06vty');
          $client = new \Nexmo\Client($basic);

          $to = $_POST['code'] . $_POST['to'];
          $from = $_POST['from'];
          $text = $_POST['message'];

          $date = Carbon\Carbon::now();
          $date_now = $date->toDateString();

          $this->model('Message');

          $message_send = Message::create([
            'to' => $to,
            'from' => $from,
            'message' => $text,
            'sent_at' => $date,
            'date_sent' => $date_now,
            'sender' => $_SESSION["phone_number"],
            'sender_name' => $_SESSION["name"],
            'read' => 0
          ]);

          /** Comment this if you want to send text */
          // $this->redirect('admin/profile', 'success-reply');

          /** Uncomment this if you want to send text */
          /** or comment this if you want to store only in the database. line 456 - 470 */
          if ($message_send) {

            $message = $client->message()->send([
              'to' => $to,
              'from' => $from,
              'text' => $text
            ]);

            $this->view('admin/message-form', []);
          }else {

            $this->view('admin/message-form', []);
          }

        }else {

          $this->view('admin/message-form', []);
        }
      } else {

        $this->view('admin/message-form', []); 
      }
    } catch (Exception $e) {
      
      echo "The message was not sent. Error: " . $e->getMessage() . "\n"; 
    }

  }

  public function reply() {
    require_once "../vendor/autoload.php";

    try {

      if ( isset($_POST['send']) && $_SERVER["REQUEST_METHOD"] == "POST" ) {
      
        if (!empty($_POST['to'])) {
          
          $basic  = new \Nexmo\Client\Credentials\Basic('af4a0027', 'YJFj84sg3Ur06vty');
          $client = new \Nexmo\Client($basic);

          $this->model('Message');

          $reply = Message::where('sender', $_POST['to'])->first(); 

          $to = $reply->sender;
          $from = $reply->from;
          $text = $_POST['message'];

          $date = Carbon\Carbon::now();
          $date_now = $date->toDateString();

          $message_send = Message::create([
            'to' => $to,
            'from' => $from,
            'message' => $text,
            'sent_at' => $date,
            'date_sent' => $date_now,
            'sender' => $_SESSION["phone_number"],
            'sender_name' => $_SESSION["name"],
            'read' => 0
          ]);

          /** Comment this if you want to send text */
          $this->redirect('admin/profile', 'success-reply');

          /** Uncomment this if you want to send text */
          /** or comment this if you want to store only in the database. line 524 - 538  */
          // if ($message_send) {

          //   $message = $client->message()->send([
          //     'to' => $to,
          //     'from' => $from,
          //     'text' => $text
          //   ]);

          //   $this->view('admin/reply-form', []);
          // }else {

          //   $this->view('admin/reply-form', []);
          // }

        }else {

          $this->redirect('admin/profile');
        }
      } else {

        if( isset($_GET['id']) &&
            isset($_GET['sender']) &&
            isset($_GET['from'])
        ) {

          $this->view('admin/reply-form', [
            'id' => $_GET['id'],
            'number' => $_GET['sender'],
            'from' => $_GET['from']
          ]);
        }else {

          $this->redirect('admin/profile');
        }
      }
    } catch (Exception $e) {
      
      echo "The message was not sent. Error: " . $e->getMessage() . "\n"; 
    }

  }

  public function sent() {

    // Checks authentication
    if ( !isset($_SESSION['user']) ) {
      $this->redirect('/');
    }

    $this->model('Message');

    $messages = Message::where('sender', $_SESSION['phone_number'])->orderBy('sent_at', 'desc')->get();

    $this->view('admin/sent', [
      'messages' => $messages
    ]);

  }

  public function sentMessage($id='') {

    // Checks authentication
    if ( !isset($_SESSION['user']) ) {
      $this->redirect('/');
    }

    $this->model('Message');

    // Gets message content
    $message = Message::where('id', $id)->first();

    $this->view('admin/sent-message', [
      'message' => $message
    ]);

  }

  public function inbox() {

    // Checks authentication
    if ( !isset($_SESSION['user']) ) {
      $this->redirect('/');
    }

    $this->model('Message');

    $messages = Message::where('to', $_SESSION['phone_number'])->orderBy('sent_at', 'desc')->get();

    $this->view('admin/inbox', [
      'messages' => $messages
    ]);

  }

  public function inboxMessage($id='') {

    // Checks authentication
    if ( !isset($_SESSION['user']) ) {
      $this->redirect('/');
    }

    $this->model('Message');

    // Update read
    Message::where('id', $id)->update([
      'read' => 1,
      'seen' => Carbon\Carbon::now()
    ]);

    // Gets message content
    $message = Message::where('id', $id)->first();

    $this->view('admin/inbox-message', [
      'message' => $message
    ]);

  }

  public function schedule($id='') {

    // Checks authentication
    if ( !isset($_SESSION['user']) ) {
      $this->redirect('/');
    }

    $this->model('Applicant');

    if ( isset($_POST['set']) && $_SERVER["REQUEST_METHOD"] == "POST" ) {

      $set_id = $_POST['id'];
      $schedule_date = $_POST['schedule'];

      Applicant::where('id', $set_id)->update([
        'schedule' => $schedule_date,
        'remarks' => 'Hired'
      ]);

      $this->redirect('admin/profile', 'success-set-schedule');

    }else {

      $this->view('admin/schedule-form', [
        'id' => $id
      ]);
    }

  }
  
}