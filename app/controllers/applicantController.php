<?php

// starts session.
session_start();

class ApplicantController extends Controller {

  public function index() {
    
    $this->view('applicant/login', []);

  }

  public function profile() {

    // If the user is signing out.
    if (isset($_POST['logout'])) {

      session_destroy();

      $this->redirect('/', 'logout');
    }

    /** Access tables **/

    // Access table for Administrators.
    $this->model('Administrator');

    // Access table for Applicants
    $this->model('Applicant');

    // Access table for Messages
    $this->model('Message');

    /** Queries **/

    // Load list of Recruiter in the profile of the applicant.
    $administrator = Administrator::where('admin', 0)->orderBy('created_at', 'desc')->get();

    // If the user is signing in.
    if ( isset($_POST['login']) && $_SERVER["REQUEST_METHOD"] == "POST" ) {
      
      // Access table administrators.
      $this->model('Applicant');

      $username = $_POST['username'];
      $password = $_POST['password'];

      $user = Applicant::where([
        'username' => $username
      ])->first();

      // Get inbox count
      $message_count = Message::where('read', 0)->where('to', $user->phone_number)->count();

      // Checks if the username is in the system.
      if ( !password_verify($password, $user->password) && empty($user) ) {

        // Redirects to the login page of the Admin.
        $this->redirect('applicant/', 'login-error');
      } else {

        $_SESSION["user"] = $user->id;

        // user's name
        $_SESSION["name"] = $user->first_name . ' ' . $user->last_name ;

        // user's number
        $_SESSION["phone_number"] = $user->phone_number;

        $this->view('applicant/profile', [
          'user' => $user,
          'administrator' => $administrator,
          'message-count' => $message_count
        ]);
      }
    }

    // Checks authentication
    if ( !isset($_SESSION['user']) ) {
      $this->redirect('/');
    }
    
    // For redirecting to the profile page or user's main page.
    if (!empty($_SESSION['user'])) {

      $user = Applicant::find($_SESSION['user']);

      // Get inbox count
      $message_count = Message::where('read', 0)->where('to', $_SESSION['phone_number'])->count();
      
      $this->view('applicant/profile', [
        'user' => $user,
        'administrator' => $administrator,
        'message-count' => $message_count
      ]);
    }

  }

  public function addRecruiter() {

    // Checks authentication
    if ( !isset($_SESSION['user']) ) {
      $this->redirect('/');
    }

    // define variables and set to empty values
    // For errors
    $first_nameErr = $last_nameErr = $middle_nameErr = "";
    $phone_numberErr = $passwordErr = "";

    // empty values
    $first_name = $last_name = $middle_name = "";
    $phone_number = $password = "";

    // for checking of number of existing usernames
    $username_exist = false;

    if ( isset($_POST['save']) && $_SERVER["REQUEST_METHOD"] == "POST" ) {

      require_once '../app/services/filters.php';

      if (empty($_POST["first_name"])) {
        
        $first_nameErr = "First name is required";
      } else {
        
        $first_name = test_input($_POST["first_name"]);
        
        // check if name only contains letters and whitespace
        if (!preg_match("/^[a-zA-Z ]*$/",$first_name)) {
        
          $first_nameErr = "Only letters and white space allowed"; 
        }
      }

      if (empty($_POST["last_name"])) {
        
        $last_nameErr = "Last name is required";
      } else {
        
        $last_name = test_input($_POST["last_name"]);
        
        // check if name only contains letters and whitespace
        if (!preg_match("/^[a-zA-Z ]*$/",$last_name)) {
        
          $last_nameErr = "Only letters and white space allowed"; 
        }
      }

      $username = test_input($_POST['phone_number']);
      $phone_number = test_input($_POST['phone_number']);
      $password = password_hash($_POST['password'], PASSWORD_DEFAULT);

      $this->model('Recruiter');

      $verify_username = Recruiter::where('username', $username)->get();

      if ((empty($verify_username)) &&
          !empty($_POST['first_name']) && 
          !empty($_POST['last_name']) &&
          !empty($_POST['phone_number'])) {
            
        // create new recruiter
        Recruiter::create([
          'username' => $phone_number,
          'password' => $password,
          'first_name' => $first_name,
          'last_name' => $last_name,
          'middle_name' => $middle_name,
          'phone_number' => $phone_number,
          'remarks' => 'Pending'
        ]);

        $this->redirect('applicant/profile');
      }else {

        if (!empty($verify_username)) {
          $username_exist = true;
        }

        $this->view('applicant/recruiter-add', [
          'first_nameErr' => $first_nameErr,
          'last_nameErr' => $last_nameErr,
          'phone_numberErr' => $phone_numberErr,
          'username_exist' => $username_exist
        ]); 
      }
    }else {

      $this->view('applicant/recruiter-add', [
        'first_nameErr' => $first_nameErr,
        'last_nameErr' => $last_nameErr,
        'phone_numberErr' => $phone_numberErr,
        'username_exist' => $username_exist
      ]); 
    }

  }

  public function listRecruiters() {

    // Checks authentication
    if ( !isset($_SESSION['user']) ) {
      $this->redirect('/');
    }

    $this->model('Recruiter');

    $recruiters = Recruiter::orderBy('created_at', 'desc')->get();

    $this->view('applicant/list-recruiters', [
      'recruiters' => $recruiters
    ]);
     
  }

  public function listAdmins() {

    // Checks authentication
    if ( !isset($_SESSION['user']) ) {
      $this->redirect('/');
    }

    $this->model('Administrator');

    $administrators = Administrator::where('admin', 0)->orderBy('created_at', 'desc')->get();

    $this->view('applicant/list-admins', [
      'administrators' => $administrators
    ]);

  }

  public function send() {
    require_once "../vendor/autoload.php";

    try {

      if ( isset($_POST['send']) && $_SERVER["REQUEST_METHOD"] == "POST" ) {
      
        if (!empty($_POST['to'])) {
          
          $basic  = new \Nexmo\Client\Credentials\Basic('af4a0027', 'YJFj84sg3Ur06vty');
          $client = new \Nexmo\Client($basic);

          $to = $_POST['code'] . $_POST['to'];
          $from = $_POST['from'];
          $text = $_POST['message'];

          $date = Carbon\Carbon::now();
          $date_now = $date->toDateString();

          $this->model('Message');

          $message_send = Message::create([
            'to' => $to,
            'from' => $from,
            'message' => $text,
            'sent_at' => $date,
            'date_sent' => $date_now,
            'sender' => $_SESSION["phone_number"],
            'sender_name' => $_SESSION["name"],
            'read' => 0
          ]);

          /** Comment this if you want to send text */
          // $this->redirect('applicant/profile', 'success-reply');

          /** Uncomment this if you want to send text */
          /** or comment this if you want to store only in the database */
          if ($message_send) {

            $message = $client->message()->send([
              'to' => $to,
              'from' => $from,
              'text' => $text
            ]);

            $this->view('applicant/message-form', []);
          }else {

            $this->view('applicant/message-form', []);
          }
        }else {

          $this->view('applicant/message-form', []);
        }
      } else {

        $this->view('applicant/message-form', []); 
      }
    } catch (Exception $e) {
      
      echo "The message was not sent. Error: " . $e->getMessage() . "\n";
    }
    
  }

  public function reply() {
    require_once "../vendor/autoload.php";

    try {

      if ( isset($_POST['send']) && $_SERVER["REQUEST_METHOD"] == "POST" ) {
      
        if (!empty($_POST['to'])) {
          
          $basic  = new \Nexmo\Client\Credentials\Basic('af4a0027', 'YJFj84sg3Ur06vty');
          $client = new \Nexmo\Client($basic);

          $this->model('Message');

          $reply = Message::where('sender', $_POST['to'])->first(); 

          $to = $reply->sender;
          $from = $reply->from;
          $text = $_POST['message'];

          $date = Carbon\Carbon::now();
          $date_now = $date->toDateString();

          $message_send = Message::create([
            'to' => $to,
            'from' => $from,
            'message' => $text,
            'sent_at' => $date,
            'date_sent' => $date_now,
            'sender' => $_SESSION["phone_number"],
            'sender_name' => $_SESSION["name"],
            'read' => 0
          ]);

          /** Comment this if you want to send text */
          $this->redirect('applicant/profile', 'success-reply');

          /** Uncomment this if you want to send text */
          /** or comment this if you want to store only in the database */
          // if ($message_send) {

          //   $message = $client->message()->send([
          //     'to' => $to,
          //     'from' => $from,
          //     'text' => $text
          //   ]);

          //   $this->view('admin/reply-form', []);
          // }else {

          //   $this->view('admin/reply-form', []);
          // }
        }else {

          $this->redirect('applicant/profile');
        }
      } else {

        if( isset($_GET['id']) &&
            isset($_GET['sender']) &&
            isset($_GET['from'])
        ) {

          $this->view('applicant/reply-form', [
            'id' => $_GET['id'],
            'number' => $_GET['sender'],
            'from' => $_GET['from']
          ]);
        }else {

          $this->redirect('applicant/profile');
        }
      }
    } catch (Exception $e) {
      
      echo "The message was not sent. Error: " . $e->getMessage() . "\n"; 
    }

  }

  public function sent() {

    // Checks authentication
    if ( !isset($_SESSION['user']) ) {
      $this->redirect('/');
    }

    $this->model('Message');

    $messages = Message::where('sender', $_SESSION['phone_number'])->orderBy('sent_at', 'desc')->get();

    $this->view('applicant/sent-messages', [
      'messages' => $messages
    ]);

  }

  public function sentMessage($id='') {

    // Checks authentication
    if ( !isset($_SESSION['user']) ) {
      $this->redirect('/');
    }

    $this->model('Message');

    $message = Message::where('id', $id)->first();

    $this->view('applicant/view-message', [
      'message' => $message
    ]);

  }

  public function inbox() {

    // Checks authentication
    if ( !isset($_SESSION['user']) ) {
      $this->redirect('/');
    }

    $this->model('Message');

    $messages = Message::where('to', $_SESSION['phone_number'])->orderBy('sent_at', 'desc')->get();

    $this->view('applicant/inbox', [
      'messages' => $messages
    ]);

  }

  public function inboxMessage($id='') {

    // Checks authentication
    if ( !isset($_SESSION['user']) ) {
      $this->redirect('/');
    }

    $this->model('Message');

    // Update read
    Message::where('id', $id)->update([
      'read' => 1,
      'seen' => Carbon\Carbon::now()
    ]);

    // Gets message content
    $message = Message::where('id', $id)->first();

    $this->view('applicant/inbox-message', [
      'message' => $message
    ]);

  }

  public function schedule($id='') {

    // Checks authentication
    if ( !isset($_SESSION['user']) ) {
      $this->redirect('/');
    }

    $this->model('Administrator');

    if ( isset($_POST['set']) && $_SERVER["REQUEST_METHOD"] == "POST" ) {

      $set_id = $_POST['id'];
      $schedule_date = $_POST['schedule'];

      Administrator::where('id', $set_id)->update([
        'schedule' => $schedule_date,
        'remarks' => 'Hired'
      ]);

      $this->redirect('applicant/profile', 'success-set-schedule');

    }else {

      $this->view('applicant/schedule-form', [
        'id' => $id
      ]);
    }

  }

}