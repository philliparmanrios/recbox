<?php

use Illuminate\Database\Eloquent\Model as Eloquent;

class Message extends Eloquent {

  public $timestamps = [];

  protected $fillable = ['to', 'from', 'message', 'sent_at', 'date_sent', 'sender', 'sender_name', 'read', 'seen'];  
}