<?php

use Illuminate\Database\Eloquent\Model as Eloquent;

class Recruiter extends Eloquent {

  public $timestamps = ['created_at', 'updated_at'];

  protected $fillable = ['first_name', 'last_name', 'middle_name', 'username', 'password', 'phone_number', 'remarks'];  
}