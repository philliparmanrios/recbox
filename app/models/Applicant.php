<?php

use Illuminate\Database\Eloquent\Model as Eloquent;

class Applicant extends Eloquent {

  public $timestamps = ['created_at', 'updated_at'];

  protected $fillable = ['first_name', 'last_name', 'middle_name', 'username', 'password', 'desired_position', 'phone_number'];  
}