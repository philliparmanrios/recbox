<?php

use Illuminate\Database\Eloquent\Model as Eloquent;

class Administrator extends Eloquent {

  public $timestamps = ['created_at', 'updated_at'];

  protected $fillable = ['first_name', 'last_name', 'middle_name', 'username', 'password', 'position', 'phone_number', 'admin'];  
}